# Séminaire IPV6

## Résumé

Vous avez déjà entendu parlé d'addresse ip ressemblant à des trucs comme ça 
192.168.1.27. Ça c'est l'ipv4 et comme le pétrole, il y en a plus beaucoup dans 
le monde. Du coup, il existe une nouvelle manière d'addresser les machines plus 
modernes qui s'appelle l'ipv6. Au dela de résoudre ces problème d'amenuisement, 
l'ipv6 est un protocole beaucoup plus moderne que son predecesseur et qui permet 
entre autre l'autoconfiguration des addresses d'une machine sans serveur dhcp.
